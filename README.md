# StravaWeb

This is an Android application that lets you connect to your Strava account. It is free from any **Google services dependencies**.

It is basically a webview with various enhancements (you can hide ads banners, edit activity easily…).

![screenshot](/uploads/1018c0ef0bd3ff7113825836e2854767/Screenshot_20190616-001352_StravaWeb.png)

It is not *yet* deployed on any store.
