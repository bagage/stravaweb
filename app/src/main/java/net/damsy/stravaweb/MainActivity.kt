package net.damsy.stravaweb

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.webkit.*
import android.widget.Toast
import com.leinardi.android.speeddial.SpeedDialActionItem
import com.leinardi.android.speeddial.SpeedDialView
import kotlinx.android.synthetic.main.activity_main.*
import mu.KLogging
import java.util.*
import kotlin.concurrent.schedule

enum class Settings(val code: Int) {
    DisplayBanner(1),
    DisplayAthleteInfo(2),
    DisplayPremium(3),
    DisplayChallenges(4)
}

class StravaWebView(private val activity: MainActivity) : WebViewClient() {
    companion object : KLogging()

    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
        logger.warn { "shouldOverrideUrlLoading: url=$url" }
        // fixme: not working for google oAuth etc.
        // https://auth0.com/blog/google-blocks-oauth-requests-from-embedded-browsers/
//        if (!isStrava(url)) {
//            Intent(Intent.ACTION_VIEW, Uri.parse(url)).apply {
//                startActivity(activity, this, null)
//            }
//            return true
//        }
        return false

    }

    private fun isStrava(url: String): Boolean {
        return (Uri.parse(url).host == "www.strava.com")
    }

    override fun onPageStarted(view: WebView, url: String?, favicon: Bitmap?) {
        activity.loading.visibility = View.VISIBLE
        logger.info { "onPageStarted: url=$url" }
        toggleSettingsVisibility(view)
    }

    override fun onLoadResource(view: WebView, url: String?) {
        toggleSettingsVisibility(view)
    }
    override fun onPageFinished(view: WebView, url: String) {
        activity.loading.visibility = View.GONE
        logger.debug { "onPageFinished: url=$url" }
        if (isStrava(url)) {
            toggleSettingsVisibility(view)
            activity.swipeLayout.isRefreshing = false
        }
    }

    fun toggleSettingsVisibility(view: WebView) {
        val blocks = mapOf(
                Settings.DisplayBanner to listOf(Pair(".mobile-download-app-tout", "show"), Pair(".under-mobile-download-app-tout", "paddingTop"), Pair(".smartbanner-content", "show")),
                Settings.DisplayPremium to listOf(Pair(".card.card-group", "show"), Pair(".card.promo.promo-fancy.feed-entry", "show")),
                Settings.DisplayAthleteInfo to listOf(Pair("#athlete-profile.card", "show")),
                Settings.DisplayChallenges to listOf(Pair(".card.challenge", "show"))
        )
        blocks.forEach { b ->
            val enabled = activity.isSettingsEnabled(b.key)
            logger.info { "${if (enabled) "Showing" else "Hiding"} ${b.key}…" }
            b.value.forEach {
                executeJavascript(view, applyOnJsElement(it.first, if (enabled) it.second else "no${it.second}"))
            }
        }
    }

    private fun applyOnJsElement(selector: String, action: String): String {
        var js = "document.querySelectorAll('$selector').forEach(element => "
        when (action) {
            "noshow" -> js += "element.style.display='none'"
            "show" -> js += "element.style.display=null"
            "nopaddingTop" -> js += "element.style.paddingTop = 0"
            "paddingTop" -> js += "element.style.paddingTop = null"
        }
        js += ")"
        return js
    }

    private fun executeJavascript(view: WebView, javascript: String) {
        logger.debug("executeJavascript: $javascript")
        view.loadUrl("javascript:(function() {${javascript.trimMargin()} })()")
    }
}

class StravaChromeClient(private val activity: MainActivity) : WebChromeClient() {
    companion object : KLogging()

    var filePathCallback: ValueCallback<Array<Uri>>? = null

    override fun onShowFileChooser(webView: WebView?, filePathCallback: ValueCallback<Array<Uri>>, fileChooserParams: FileChooserParams): Boolean {
        logger.info { "onShowFileChooser" }
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.filePathCallback = filePathCallback
            activity.startActivityForResult(fileChooserParams.createIntent(), MainActivity.ACTIVITY_RESULT_FILE_CHOOSER)
            true
        } else {
            super.onShowFileChooser(webView, filePathCallback, fileChooserParams)
        }
    }
}

class MainActivity : AppCompatActivity(), SpeedDialView.OnActionSelectedListener {
    companion object : KLogging() {
        const val ACTIVITY_RESULT_FILE_CHOOSER = 42
    }

    private var readyToExit: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val activity = this
        webview.apply {
            settings.apply {
                javaScriptEnabled = true
                domStorageEnabled = true
                allowFileAccess = true
                cacheMode = if (isNetworkAvailable()) WebSettings.LOAD_DEFAULT else WebSettings.LOAD_CACHE_ELSE_NETWORK
                setAppCacheEnabled(true)
                setAppCachePath(applicationContext.cacheDir.path)
            }
            webChromeClient = StravaChromeClient(activity)
            webViewClient = StravaWebView(activity)
            if (savedInstanceState != null) {
                restoreState(savedInstanceState)
            } else {
                loadUrl("https://www.strava.com/dashboard")
            }
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            webview.setOnScrollChangeListener { _, _, y, _, _ ->
                swipeLayout.isEnabled = y == 0 && webview.url == "https://www.strava.com/dashboard"
            }
        } else {
            swipeLayout.isEnabled = false
        }

        swipeLayout.setOnRefreshListener {
            if (isNetworkAvailable())
                webview.reload()
            else
                swipeLayout.isRefreshing = false
        }

        setupFabMenu()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (requestCode == ACTIVITY_RESULT_FILE_CHOOSER) {
                (webview.webChromeClient!! as StravaChromeClient).apply {
                    filePathCallback?.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data))
                    filePathCallback = null
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        webview.saveState(outState)
    }

    @SuppressLint("ResourceType")
    fun setupFabMenu() {
        val items = mapOf(
                Settings.DisplayAthleteInfo to "athlete info",
                Settings.DisplayPremium to "premium ads",
                Settings.DisplayBanner to "mobile app banner",
                Settings.DisplayChallenges to "challenges"
        )
        items.forEach {
            speedDial.addActionItem(SpeedDialActionItem.Builder(it.key.code, R.drawable.ic_settings).setLabel("${if (isSettingsEnabled(it.key)) "Hide" else "Show"} ${it.value}").create())
        }
        speedDial.setOnActionSelectedListener(this)
    }

    override fun onActionSelected(actionItem: SpeedDialActionItem?): Boolean {
        val key = "settings_${actionItem?.id}"
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val value = !sharedPref.getBoolean(key, false)
        logger.info("updating preferences $key to $value")
        with(sharedPref.edit()) {
            putBoolean(key, value)
            apply()
        }
        setupFabMenu()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            (webview.webViewClient as StravaWebView).toggleSettingsVisibility(webview)
        } else {
            webview.reload()
        }
        return false
    }

    fun isSettingsEnabled(settings: Settings): Boolean {
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        return sharedPref.getBoolean("settings_${settings.code}", false)
    }

    override fun onBackPressed() {
        when {
            webview.canGoBack() -> webview.goBack()
            readyToExit -> super.onBackPressed()
            else -> {
                readyToExit = true
                Toast.makeText(this, "Please press back again to exit.", Toast.LENGTH_SHORT).show()
                Timer().schedule(3000) {
                    readyToExit = false
                }
            }
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo?.isConnected ?: false
    }
}
